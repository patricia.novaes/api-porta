package br.com.novapat.api.porta.repository;

import br.com.novapat.api.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Long> {


}
