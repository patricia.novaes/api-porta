package br.com.novapat.api.porta.controller;


import br.com.novapat.api.porta.models.Porta;
import br.com.novapat.api.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping("/porta")
    public Porta criarPorta (@RequestBody Porta porta){
        return portaService.create(porta);
    }

    @GetMapping("porta/{id}")
    public Optional<Porta> buscarPorId(@PathVariable Long id){
        return portaService.findById(id);
    }

}
