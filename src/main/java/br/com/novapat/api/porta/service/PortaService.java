package br.com.novapat.api.porta.service;

import br.com.novapat.api.porta.models.Porta;
import br.com.novapat.api.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta create (Porta porta){
        return portaRepository.save(porta);
    }

    public Optional<Porta> findById(Long id) {
        return portaRepository.findById(id);
    }
}
