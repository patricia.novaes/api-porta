package br.com.novapat.api.porta;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableFeignClients
public class ApiPortaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPortaApplication.class, args);
	}

}
